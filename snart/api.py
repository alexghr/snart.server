from tastypie.resources import ModelResource
from tastypie import fields
from tastypie.authentication import ApiKeyAuthentication, SessionAuthentication, MultiAuthentication
from tastypie.authorization import ReadOnlyAuthorization
from tastypie.serializers import Serializer

from models import Station, Link, TransportLine, TransportType


class SnartResource(ModelResource):

    class Meta:
        authentication = MultiAuthentication(ApiKeyAuthentication(), SessionAuthentication())
        authorization = ReadOnlyAuthorization()
        default_format = 'application/json'
        serializer = Serializer(formats=['json'])

        list_allowed_methods = ['get']
        detail_allowed_methods = ['get']

        always_return_data = True
        export_allowed_methods = []


class TypeResource(SnartResource):

    class Meta(SnartResource.Meta):
        queryset = TransportType.objects.all()


class LineResource(SnartResource):

    type = fields.ToOneField(TypeResource, 'type')
    #link_set = fields.ToManyField('snart.api.LinkResource', 'link_set', related_name='line')

    class Meta(SnartResource.Meta):
        queryset = TransportLine.objects.all()


class LinkResource(SnartResource):

    line = fields.ToOneField('snart.api.LineResource', 'line')
    station = fields.ToOneField('snart.api.StationResource', 'station')

    class Meta(SnartResource.Meta):
        queryset = Link.objects.all()


class StationResource(SnartResource):

    #link_set = fields.ToManyField('snart.api.LinkResource', 'link_set', related_name='station')
    types = fields.ToManyField('snart.api.TypeResource', 'types')

    class Meta(SnartResource.Meta):
        queryset = Station.objects.select_related('station', 'link')
