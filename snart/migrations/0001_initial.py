# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'TransportType'
        db.create_table(u'snart_transporttype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=16)),
        ))
        db.send_create_signal(u'snart', ['TransportType'])

        # Adding model 'TransportLine'
        db.create_table(u'snart_transportline', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['snart.TransportType'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=8)),
            ('night_line', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'snart', ['TransportLine'])

        # Adding model 'Station'
        db.create_table(u'snart_station', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('latitude', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=8)),
            ('longitude', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=8)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
        ))
        db.send_create_signal(u'snart', ['Station'])

        # Adding M2M table for field types on 'Station'
        db.create_table(u'snart_station_types', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('station', models.ForeignKey(orm[u'snart.station'], null=False)),
            ('transporttype', models.ForeignKey(orm[u'snart.transporttype'], null=False))
        ))
        db.create_unique(u'snart_station_types', ['station_id', 'transporttype_id'])

        # Adding model 'Link'
        db.create_table(u'snart_link', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('line', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['snart.TransportLine'])),
            ('station', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['snart.Station'])),
            ('index', self.gf('django.db.models.fields.IntegerField')()),
            ('direction', self.gf('django.db.models.fields.CharField')(max_length=1)),
        ))
        db.send_create_signal(u'snart', ['Link'])


    def backwards(self, orm):
        # Deleting model 'TransportType'
        db.delete_table(u'snart_transporttype')

        # Deleting model 'TransportLine'
        db.delete_table(u'snart_transportline')

        # Deleting model 'Station'
        db.delete_table(u'snart_station')

        # Removing M2M table for field types on 'Station'
        db.delete_table('snart_station_types')

        # Deleting model 'Link'
        db.delete_table(u'snart_link')


    models = {
        u'snart.link': {
            'Meta': {'object_name': 'Link'},
            'direction': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index': ('django.db.models.fields.IntegerField', [], {}),
            'line': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['snart.TransportLine']"}),
            'station': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['snart.Station']"})
        },
        u'snart.station': {
            'Meta': {'object_name': 'Station'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '8'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '8'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'transport_lines': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['snart.TransportLine']", 'through': u"orm['snart.Link']", 'symmetrical': 'False'}),
            'types': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['snart.TransportType']", 'symmetrical': 'False'})
        },
        u'snart.transportline': {
            'Meta': {'object_name': 'TransportLine'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'night_line': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['snart.TransportType']"})
        },
        u'snart.transporttype': {
            'Meta': {'object_name': 'TransportType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '16'})
        }
    }

    complete_apps = ['snart']