# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Link.street'
        db.add_column(u'snart_link', 'street',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=256),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Link.street'
        db.delete_column(u'snart_link', 'street')


    models = {
        u'snart.link': {
            'Meta': {'object_name': 'Link'},
            'direction': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index': ('django.db.models.fields.IntegerField', [], {}),
            'line': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['snart.TransportLine']"}),
            'station': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['snart.Station']"}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        u'snart.station': {
            'Meta': {'object_name': 'Station'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '8'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '8'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'transport_lines': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['snart.TransportLine']", 'through': u"orm['snart.Link']", 'symmetrical': 'False'}),
            'types': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['snart.TransportType']", 'symmetrical': 'False'})
        },
        u'snart.transportline': {
            'Meta': {'object_name': 'TransportLine'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'night_line': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['snart.TransportType']"})
        },
        u'snart.transporttype': {
            'Meta': {'object_name': 'TransportType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '16'})
        }
    }

    complete_apps = ['snart']