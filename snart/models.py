from django.db import models
from django.contrib.auth.models import User
from tastypie.models import create_api_key


class TransportType(models.Model):

    name = models.CharField(max_length=16, unique=True)

    def __unicode__(self):
        return self.name


class TransportLine(models.Model):

    type = models.ForeignKey(TransportType)
    name = models.CharField(max_length=8)
    night_line = models.BooleanField()

    def __unicode__(self):
        return self.name


class Station(models.Model):

    latitude = models.DecimalField(max_digits=10, decimal_places=8)
    longitude = models.DecimalField(max_digits=10, decimal_places=8)
    name = models.CharField(max_length=128)
    transport_lines = models.ManyToManyField(TransportLine, through='Link')
    types = models.ManyToManyField(TransportType)

    def __unicode__(self):
        return self.name


class Link(models.Model):

    DIRECTION_FWD = 'T'
    DIRECTION_BWD = 'R'

    DIRECTION_CHOICES = (
        (DIRECTION_FWD, 'Tur'),
        (DIRECTION_BWD, 'Retur')
    )

    line = models.ForeignKey(TransportLine)
    station = models.ForeignKey(Station)
    index = models.IntegerField()
    direction = models.CharField(max_length=1, choices=DIRECTION_CHOICES)
    street = models.CharField(max_length=256)

    def __unicode__(self):
        return self.station.name + ' ' + self.line.name + ' ' + self.direction


models.signals.post_save.connect(create_api_key, sender=User)
