from django.conf.urls import patterns, include, url
from django.contrib import admin

from tastypie.api import Api

from snart.api import StationResource, LinkResource, LineResource, TypeResource

admin.autodiscover()

api_v1 = Api(api_name='v1')
api_v1.register(TypeResource())
api_v1.register(LineResource())
api_v1.register(LinkResource())
api_v1.register(StationResource())

urlpatterns = patterns(
    '',
    url(r'^api/', include(api_v1.urls)),
    url(r'^admin/', include(admin.site.urls))
)
